package main

import (
	"fmt"
)

type user struct {
	name string
	age  int
	*child
}

type child struct{
	name string
	age int
}

func main() {
	user1:=&user{
		name:"corerman",
		age:68,
		child:&child{
			name:"libiao",
			age:25,
		},
	}
	user1.showChildInfo()
}

func (child *child) showChildInfo(){
	fmt.Printf("%s : %d",child.name,child.age);
}