package main

import(
	"fmt"
	"math/rand"
	"sort"
)

func main() {
	numlist := []int{1, 2, 3, 4, 5} //this is a slice
	fmt.Println(numlist)

	numlist2 := [...]int{1, 2, 3, 4, 5} // this is a array
	fmt.Println(numlist2)
	//numlist2=append(numlist2,1);

	numlist=append(numlist, 6)  //this is append function
	fmt.Println(numlist)

	//slice内存分配机制：小于1024情况下，每次会扩容为原来的2倍，高于1024后，会成为原来的1/4
	scores := make([]int, 0, 5)
	c := cap(scores)
	fmt.Println(c)
	for i := 0; i < 25; i++ {
		scores = append(scores, i)
		// if our capacity has changed,
		// Go had to grow our array to accommodate the new data
		if cap(scores) != c {
			c = cap(scores)
			fmt.Println(c)
		}
	}

	//slice之间相互赋值仅仅是指针引用
	numlist3:=[]int{1,2,3,4,5}  //[1,2,3,4,5]
	numlist4:=numlist3[3:5]   //[4,5]
	fmt.Println(numlist4)
	numlist4[0]=0          //[0,5]
	fmt.Println(numlist4) // [0,5]
	fmt.Println(numlist3) //[1,2,3,0,5]

	//slice的四种定义方法
	defineSlice()

	//slice的 for range循环
	for index,value:=range numlist3{
		fmt.Printf("%d==%d\n",index,value)
	}

	//slice use function
	sortSlice()

	//slice copy
	numlist5:=make([]int,len(numlist3),len(numlist3))
	copy(numlist5,numlist3)
	fmt.Println(numlist5)

}

func defineSlice(){
	//切片的四种定义方法
	numlist1:=[]int{1,2,3,4,5}
	var numlist2 []int //注意var标识 如果有赋值操作则不需要var，否则需要var
	numlist2=[]int{5,5,5,5,5}
	numlist3:=make([]int,3,10)

	fmt.Println(numlist1)
	fmt.Println(numlist2)
	fmt.Println(numlist3)
}

func sortSlice(){
	var numlist []int
	for i:=1;i<100;i++{
		numlist=append(numlist, rand.Int()%100);
	}
	fmt.Println(numlist)
	sort.Ints(numlist)  //切片是指针引用
	fmt.Println(numlist)
}
