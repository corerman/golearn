package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func main() {
	mapElementList := make(map[int]string)  //map的创建
	sand := time.Now().Unix()  //产生当前时间戳
	randsend := rand.New(rand.NewSource(sand))  //根据新的时间戳生成新的随机源
	for i := 1; i <= 6; i++ {
		mapElementList[i] = strconv.Itoa(randsend.Int() % 10) //字符串转换
	}

	var code string = ""
	for _, value := range mapElementList {
		code += value   //生成6位验证码
	}
	fmt.Println(code)
	fmt.Println(mapElementList)

	//Map初始化，并分配初始化存储大小
	mapElementList2:=make(map[int]string,10)
	mapElementList2[1]="string1"
	//删除map元素
	delete(mapElementList2,1)
	fmt.Println(mapElementList2)

	//创建map元素体
	mapElementList3:=initMapElement()
	fmt.Println(mapElementList3)

	//创建map切片
	mapElementList4:=initMapElementSlice();
	fmt.Println(mapElementList4)

}

func initMapElementSlice()([]map[int]string){
	//创建map对象的途径
	var retinfo []map[int]string
	retinfo=append(retinfo,map[int]string{
		1:"string1",
		2:"string2",
	})
	retinfo=append(retinfo,map[int]string{
		3:"string3",
		4:"string4",
	})
	return retinfo
}


func initMapElement()(map[int]string){
	ret:=map[int]string{
		1:"string1",
		2:"string2",
	}
	return ret
}