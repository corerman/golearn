package main

import "fmt"

func main() {
	//const length array
	const len uint = 100
	var numlist [len]int
	for i := 0; i < 100; i++ {
		numlist[i] = i
	}
	fmt.Println(numlist)

	//dynamic length array 但数据必须初始化
	numlist2:=[...]uint{1,2,3,4,5,6,7,8,9}
	fmt.Println(numlist2)

	var numlist3=[...]uint{1,2,3,4,5,6,7}
	fmt.Println(numlist3)

	//array for loop
	for index,value :=range numlist3{
		fmt.Printf("%d  %d\n",index,value)
	}
}
