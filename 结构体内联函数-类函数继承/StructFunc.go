package main

import (
	"fmt"
)

type userType struct {
	name string
	age  int
	*child
}

type userType2 struct {
	name string
	age  int
	child *child
}

type child struct{
	name string
	age int
}

func main() {
	user1:=&userType{
		name:"corerman",
		age:68,
		child:&child{
			name:"libiao",
			age:25,
		},
	}
	user1.showChildInfo()  //为啥这种可以直接调用子结构体所属方法?  答：父结构体直接指针引用了子结构体

	user2:=&userType2{
		name:"corerman2",
		age:69,
		child:&child{
			name:"libiao2",
			age:26,
		},
	}

	user2.child.showChildInfo()  //为啥这种就不可以直接调用子结构体所属方法？ 答：父结构体在定义时采用了变量做引进，无法直接到子层

}

func (child *child) showChildInfo(){
	fmt.Printf("%s : %d\n",child.name,child.age);
}