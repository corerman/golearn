package main

import (
	"bufio"
	"fmt"
	"net"
	"strconv"
)

type NetAddress struct {
	host string
	port int
}

var onlineUser int = 0

func (*NetAddress) getHost(host string, port int) (hosturl string) {
	return host + ":" + strconv.Itoa(port)
}

func main() {
	netLink := new(NetAddress)
	netUrl := netLink.getHost("0.0.0.0", 9999)
	var tcpAddr *net.TCPAddr //创建一个网络地址
	tcpAddr, error := net.ResolveTCPAddr("tcp", netUrl)
	checkError(error)

	tcpLister, error := net.ListenTCP("tcp", tcpAddr) //创建TCP监听
	defer tcpLister.Close()
	for {
		tcpConn, error := tcpLister.AcceptTCP()
		if error != nil {
			continue
		}
		fmt.Println("A client connected : " + tcpConn.RemoteAddr().String())
		go tcphandle(tcpConn)

	}
	tcpLister.Accept()

}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func tcphandle(conn *net.TCPConn) {
	ipStr := conn.RemoteAddr().String()
	onlineUser++
	fmt.Printf("online user count: %d\n", onlineUser)
	defer func() {
		fmt.Println("disconnected :" + ipStr)
		onlineUser--
		fmt.Printf("online user count: %d\n", onlineUser)
		conn.Close()
	}()

	reader := bufio.NewReader(conn)
	for {
		message, err := reader.ReadString('\n')
		if err != nil {
			return
		}
		fmt.Println(string(message))
		conn.Write([]byte(message))
	}

}
