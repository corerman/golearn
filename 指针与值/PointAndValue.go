package main

import (
	"fmt"
	"strconv"
)

type userType struct {
	name string
	age  int
}

func main() {
	userList := make([]userType, 10, 10)       //初始化数值型切片
	userListPoint := make([]*userType, 10, 10) //初始化指针型切片
	fmt.Println(userList)
	fmt.Println(userListPoint)

	//对于数字型切片进行元素构造
	for i := 0; i < cap(userList); i++ {
		userList[i] = userType{
			name: "name" + strconv.Itoa(i),
			age:  i,
		}
	}
	fmt.Println(userList)

	//对于指针型切片进行元素构造
	for i := 0; i < cap(userListPoint); i++ {
		userListPoint[i] = &userType{
			name: "name" + strconv.Itoa(i),
			age:  i,
		}
	}

	fmt.Println(userListPoint)
}
